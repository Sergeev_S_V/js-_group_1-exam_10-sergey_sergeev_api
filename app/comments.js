const express = require('express');
const router = express.Router();


const createRouter = (db) => {
  router.get('/', (req, res) => {
    if (req.query.news_id) {
      db.query(
        'SELECT `author`, `comment`, `id` FROM `comments` ' +
        'WHERE post_id = ' + req.query.news_id,
        (err, result) => {
          if (err) throw err;

          res.send(result);
        })
    } else {
      db.query(
        'SELECT * FROM `comments`',
        (err, result) => {
          if (err) throw err;

          res.send(result);
        })
    }
  });

  router.post('/', (req, res) => {
    const comment = req.body;

    if (comment.comment && comment.postId) {
      if (comment.author.length === 0) {
        comment.author = 'Anonymous';
      }

      db.query(
        'INSERT INTO comments(`post_id`, `author`, `comment`)' +
        'VALUES (?,?,?)',
        [comment.postId, comment.author, comment.comment],
        (err, result) => {
          comment.id = result.insertId;
          res.send(comment);
        }
      );
    } else {
      res.send('Enter all field');
    }
  });

  router.delete('/:id', (req, res) => {
    db.query(
      'DELETE FROM `exam-10`.comments where id = ' + req.params.id,
      (err) => {
        if (err) {
          res.send(err.sqlMessage);
        } else {
          res.send('comment was deleted');
        }
      }
    );
  });

  return router;
};

module.exports = createRouter;