const express = require('express');
const router = express.Router();
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({storage});

const createRouter = (db) => {
  router.get('/', (req, res) => {
    db.query('SELECT `id`, `title`, `image`, `date`  FROM `news`', (err, result) => {
      if (err) throw err;

      res.send(result);
    })
  });

  router.get('/:id', (req, res) => {
    db.query(
      'SELECT * ' +
      'FROM `exam-10`.news WHERE id = ' + req.params.id,
      (err, result) => {
        if (err) throw err;
        if (result.length === 0) {
          res.status(404).send({err: 'Not found'});
        } else res.send(result);
      }
    );
  });

  router.post('/', upload.single('image'), (req, res) => {
    const news = req.body;

    if (news.title && news.content) {
      if (req.file) {
        news.image = req.file.filename;
      } else {
        news.image = null;
      }

      db.query(
        'INSERT INTO news (`title`, `content`, `image`)' +
        'VALUES (?,?,?)',
        [news.title, news.content, news.image],
        (err, result) => {
          news.id = result.insertId;
          res.send(news);
        }
      );
    } else {
      res.send('Enter all field');
    }
  });

  router.delete('/:id', (req, res) => {
    db.query(
      'DELETE FROM `exam-10`.news WHERE id = ' + req.params.id,
      (err) => {
        if (err) {
          res.send(err.sqlMessage);
        } else {
          res.send(null);
        }
      }
    );
  });



  return router;
};

module.exports = createRouter;