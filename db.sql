
create schema `exam-10`;
CREATE TABLE `exam-10`.`news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `caption` VARCHAR(45) NOT NULL,
  `content` TEXT NOT NULL,
  `image` VARCHAR(255) NULL,
  `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));
CREATE TABLE `exam-10`.`comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `new_id` VARCHAR(45) NOT NULL,
  `author` VARCHAR(45) NOT NULL,
  `comment` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`new_id`) REFERENCES `news`(`id`));