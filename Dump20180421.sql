-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: exam-10
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` varchar(45) NOT NULL,
  `author` varchar(45) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (2,'17','Vasya','test'),(3,'17','Vasya','comment\'s vasya'),(6,'15','vvv','vvv'),(7,'19','vasya','text xtx'),(9,'15','qwe','qweqweq'),(10,'15','qwe','qweqweqrwere'),(18,'14','author','text'),(19,'14','author','text1'),(22,'14','author','etergevtretvwe'),(23,'14','tewtwet','twqertvetwervtwevre'),(26,'21','Anonymous','wqeqw'),(28,'17','rteterterte','werwrwerwerw'),(30,'27','qweqweq','qweqweq');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (17,'hwllw','ewkrjwqerhqwherjwrmwecriqrhqcwrwcrwe','0KYE~XGqm4pXbPhkSWQfz.jpg','2018-04-21 15:55:56'),(18,'test123merktm','ekltrnckemrtnckwertcwetrw','RJXHDOW6Zc89SlQId0ypg.jpg','2018-04-21 15:56:53'),(20,'vasya','vasya erwtet',NULL,'2018-04-21 16:42:56'),(22,'werwer','rewrwrwrwerew','OIAMwu3pZyVwxgqXOLx00.jpg','2018-04-21 19:39:02'),(23,'wrewerqwerwer','werwerwerqwerqvr4r4v34r3',NULL,'2018-04-21 19:39:10'),(24,'aaaaaaaaaaaaaaaa','aaaaaaaaaaaaaaaaaa','q9S8exoXh4DqnLLYmG5M5.jpg','2018-04-21 19:39:21'),(25,'EQWEQWE','WQRWERWERW',NULL,'2018-04-21 19:39:58'),(26,'BBBB','BBB',NULL,'2018-04-21 19:40:05'),(27,'123','123','uRWwuIBoAsY8Iybq3anE7.jpg','2018-04-21 19:40:17'),(28,'qweqwe','wqeqweq','GdXDrHdySlHqw3CGVIEYi.jpg','2018-04-21 19:40:52'),(29,'bbbbbbbbbbbbbb','bbbbbbbbbbbbbb',NULL,'2018-04-21 19:44:26'),(30,'xxxxxxxxxxxxxx','xxxxxxxxxxxxxxxxx','CxdPdtxO4ofYhGgbOWDJf.jpg','2018-04-21 19:44:39');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-21 19:50:11
