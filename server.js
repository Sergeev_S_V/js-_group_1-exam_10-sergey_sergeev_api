const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const comments = require('./app/comments');
const news = require('./app/news');

const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'exam-10'
});

connection.connect((err) => {
  if (err) throw err;

  app.use('/comments', comments(connection));
  app.use('/news', news(connection));

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});



